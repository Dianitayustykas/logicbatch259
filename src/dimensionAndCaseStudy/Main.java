package dimensionAndCaseStudy;

import java.util.Scanner;

public class Main {

				static Scanner input = new Scanner(System.in);
				
				public static void main(String[] args1) {
					String answer = "Y";

					while (answer.toUpperCase().equals("Y")) {
						try {
							System.out.println(("Enter the number of case"));
							int number = input.nextInt();
							input.nextLine();

							switch (number) {
							case 1:
								Case01.Resolve1();
								break;
							case 2:
								Case01.Resolve2();
								break;
							case 3:
								Case01.Resolve3();
								break;
							case 4:
								Case01.Resolve4();
								break;
							case 5:
								Case01.Resolve5();
								break;
							default:
								System.out.println("Case is not available");
								break;
							}
						} catch (Exception e) {
							System.out.println(e.getMessage());
						}

						System.out.println();
						System.out.println("Continue ?");
						answer = input.nextLine();

					}
	}

}
