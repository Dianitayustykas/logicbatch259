package dimensionAndCaseStudy;

import java.util.Scanner;

public class no09 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int n = input.nextInt();
		
		long[] tempArray = new long [n];
		for (int i = 0; i < n; i++) {
			long isi = input.nextInt();
			tempArray[i] = isi;
		}
		long sum = 0;
		for (int i = 0; i < tempArray.length; i++) {
			sum += tempArray[i];
		}
		System.out.println(sum);
	}

}
