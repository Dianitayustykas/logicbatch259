package dimensionAndCaseStudy;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Case01 {
	public static void Resolve1() {
		Scanner input = new Scanner(System.in);
		System.out.print(" a = ");
		int a = input.nextInt();
		System.out.print("b = ");
		int b = input.nextInt();
		
		System.out.println(a + b);
	}
	
	public static void Resolve2() throws ParseException {
		Scanner input = new Scanner(System.in);
		
		System.out.print("Masukan Waktu : ");
		String A = input.nextLine().toLowerCase();
		
		DateFormat formatAwal = new SimpleDateFormat("hh:mm:ssaa");
		DateFormat formatAkhir = new SimpleDateFormat("hh:mm:ss");
	
			Date waktu = formatAwal.parse(A);
			String hasil = formatAkhir.format(waktu);
			System.out.println(hasil);
	}
	
	public static void Resolve3() {
		Scanner input = new Scanner(System.in);

		System.out.print("Kata Pertama : ");
		String kataPertama = input.nextLine();
		System.out.print("Password : ");
		int password = input.nextInt();

		String kataAkhir = "";
		char huruf;

		for (int i = 0; i < kataPertama.length(); i++) {
			huruf = kataPertama.charAt(i);
			if (huruf >= 'a' && huruf <= 'z') {
				huruf = (char) (huruf + password);
				if (huruf < 'z') {
					huruf = (char) (huruf + 'a' - 'z' - 1);
				}
				kataAkhir = kataAkhir + huruf;
			} else if (huruf >= 'A' && huruf <= 'z') {
				huruf = (char) (huruf + password);
				if (huruf > 'z') {
					huruf = (char) (huruf + 'A' - 'z' - 1);
				}
				kataAkhir = kataAkhir + huruf;
			} else {
				kataAkhir = kataAkhir + huruf;
			}
		}
		System.out.print("Kata Terakhir : " + kataAkhir);
	}
	
	
	public static int Resolve4() {
		Scanner input = new Scanner(System.in);
		System.out.print("n : ");
		int n = input.nextInt();

		int[][] nilai = new int[3][n];
		int x = 0;
		int d1 = 0, d2 = 0;
	      
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                if (i == j)
                    d1 += nilai[i][j];
      
                if (i == n - j - 1)
                    d2 += nilai[i][j];
            }
        }
        System.out.print( n);
        return Math.abs(d1-d2);
	}
	
	public static void Resolve5() {
		Scanner input = new Scanner(System.in);
		
		DecimalFormat a = new DecimalFormat("#.######");
		System.out.println("n = ");
		int n = input.nextInt();
		int[] angka = new int [n];
		
		for (int i = 0; i < n; i++) {
			int x = input.nextInt();
			angka [i] = x;
		}
		double positif = 0;
		double negatif = 0;
		double nol = 0;
		
		for (int i = 0; i < angka.length; i++) {
			if (angka[i] > 0) {
				positif++;
			}else if(angka[i] < 0) {
				negatif++;
			}else {
				nol++;
			}
			
		}
		double peluangPositif = positif/n;
		double peluangNegatif = negatif/n;
		double peluangNol = nol/n;
		
		System.out.println("Positif = " + a.format(peluangPositif));
		System.out.println("Negatif = " + a.format(peluangNegatif));
		System.out.println("Nol = " + a.format(peluangNol));
	}
}
