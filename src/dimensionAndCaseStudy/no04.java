package dimensionAndCaseStudy;

import java.util.Scanner;

public class no04 {

	public static int main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.print("n : ");
		int n = input.nextInt();

		int[][] nilai = new int[3][n];
		int x = 0;
		int d1 = 0, d2 = 0;
	      
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                // finding sum of primary diagonal
                if (i == j)
                    d1 += nilai[i][j];
      
                // finding sum of secondary diagonal
                if (i == n - j - 1)
                    d2 += nilai[i][j];
            }
        }
        System.out.print( n);
        return Math.abs(d1-d2);
	}
}
