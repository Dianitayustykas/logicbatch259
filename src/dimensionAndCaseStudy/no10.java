package dimensionAndCaseStudy;

import java.util.Scanner;

public class no10 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int panjangArray = 3;
		int arrayA[] = new int [panjangArray];
		int arrayB[] = new int [panjangArray];
		
		for (int i = 0; i < panjangArray; i++) {
			int nilaiA = input.nextInt();
			arrayA[i] = nilaiA;
		}
		for (int i = 0; i < panjangArray; i++) {
			int nilaiB = input.nextInt();
			arrayB[i] = nilaiB;
		}
		int pointA=0;
		int pointB=0;
		
		for (int i = 0; i < panjangArray; i++) {
			if (arrayA[i] > arrayB[i]) {
				pointA++;
			}else if (arrayB[i] > arrayA[i]) {
				pointB++;
			}
		}
		System.out.println(pointA + " " + pointB);
	}

}
