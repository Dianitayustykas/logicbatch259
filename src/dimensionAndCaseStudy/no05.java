package dimensionAndCaseStudy;

import java.text.DecimalFormat;
import java.util.Scanner;

public class no05 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		DecimalFormat a = new DecimalFormat("#.######");
		System.out.println("n = ");
		int n = input.nextInt();
		int[] angka = new int [n];
		
		for (int i = 0; i < n; i++) {
			int x = input.nextInt();
			angka [i] = x;
		}
		double positif = 0;
		double negatif = 0;
		double nol = 0;
		
		for (int i = 0; i < angka.length; i++) {
			if (angka[i] > 0) {
				positif++;
			}else if(angka[i] < 0) {
				negatif++;
			}else {
				nol++;
			}
			
		}
		double peluangPositif = positif/n;
		double peluangNegatif = negatif/n;
		double peluangNol = nol/n;
		
		System.out.println("Positif = " + a.format(peluangPositif));
		System.out.println("Negatif = " + a.format(peluangNegatif));
		System.out.println("Nol = " + a.format(peluangNol));
	}

}
