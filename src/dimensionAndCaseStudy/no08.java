package dimensionAndCaseStudy;

import java.util.Scanner;

public class no08 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.println("Masukan Panjang Array = ");
		int n = input.nextInt();
		int temparray[] = new int [n];
		int max = 0;
		int hitungMax = 0;
		
		for (int i = 0; i < n; i++) {
			int isi = input.nextInt();
			temparray[i] = isi;
			if (temparray[i] > max) {
				max = temparray[i];
				hitungMax = 0;
			}
			if (temparray[i] == max) {
				hitungMax++;
			}
		}
		System.out.println(hitungMax);
	}

}
