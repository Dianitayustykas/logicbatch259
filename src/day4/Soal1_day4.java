package day4;

import java.util.Scanner;

public class Soal1_day4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.print("Masukan Jarak : ");
		int tempJarak[] = new int[4];

		double total = 0;
		double bensin = 0;

		for (int i = 0; i < 4; i++) {
			int jarak = input.nextInt();
			tempJarak[i] = jarak;
		}
		input.nextLine();
		System.out.println("Masukan index jarak : ");
		String[] indexSplit = input.nextLine().split(" ");

		for (int i = 0; i < indexSplit.length; i++) {
			total += tempJarak[Integer.parseInt(indexSplit[i]) - 1];
		}
		System.out.println(total);
		bensin = total / 2500;
		System.out.println(Math.ceil(bensin));
	}
}