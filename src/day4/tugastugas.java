package day4;

import java.util.Arrays;
import java.util.Scanner;

public class tugastugas {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.println("Masukan nilai yang akan di hitung = ");

		int n = input.nextInt();
		int[] nilai = new int[n];

		for (int i = 0; i < nilai.length; i++) {
			nilai[i] = input.nextInt();
		}
		System.out.println("Data yang di Inputkan adalah ");
		for (int i : nilai) {
			System.out.println(" " + i);
		}
		Arrays.sort(nilai);
		System.out.println(Arrays.toString(nilai));
		System.out.println("Nilai Mean = " + Mean(nilai));
		System.out.println("Nilai Median = " + Median(nilai));
		System.out.println("Nilai Modus = " + Modus(nilai));
	}

	private static Float Mean(int[] nilai) {
		float rata2 = 0;
		float hasilRata2;

		for (int i : nilai) {	
			rata2 += i;
		}
		hasilRata2 = rata2 / nilai.length;
		return rata2;
	}

	private static Double Median(int[] nilai) {
		int nilaiTengah = nilai.length;
		double nilaiM;

		if (nilaiTengah == 1) {
			nilaiM = nilai[nilai.length / 2];
			return nilaiM;
		} else {
			nilaiM = (nilai[nilai.length / 2 - 1] + nilai[nilai.length / 2]);
			nilaiM = nilaiM / 2;
			return nilaiM;
		}

	}
	
	private static int Modus (int[] nilai) {
		Arrays.sort(nilai);
		int nilaiModus1 = 0;
		int nilaiModus2 = 0;
		int nilaiModus3 = nilai[0];
		int max = 0;
		for (int i:nilai) {
			if (i == nilaiModus3) {
				++nilaiModus2;
				if (nilaiModus2>max) {
					max = nilaiModus2;
					nilaiModus1 = nilaiModus3;
				}
			}else {
				nilaiModus3 = i;
				nilaiModus2 = 1;
			}
		}
		return nilaiModus1;
	}
}
