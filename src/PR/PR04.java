package PR;

import java.util.Scanner;

public class PR04 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner (System.in);
		System.out.print("Total Menu = ");
		
		int n = input.nextInt();
		int []totalMenu = new int[n];
		
		System.out.print("Makanan Alergi index ke = ");
		int indexAlergi = input.nextInt();
		
		System.out.println("Harga Menu = ");
		for (int i = 0; i < n; i++) {
			int menu = input.nextInt();
			totalMenu[i] = menu;	
		} 
		
		System.out.println("Uang Elsa = ");
		int uang = input.nextInt();
		
		int sum = 0;
		for (int num : totalMenu) {
			sum = sum + num;
		}
		
		int harusBayar = (sum - totalMenu[indexAlergi]) / 2;
		int sisaUang	= uang - harusBayar;
		
		System.out.println("Harus bayar = " + harusBayar);
		System.out.println("Sisa Uang = " + sisaUang);		
	}
}
