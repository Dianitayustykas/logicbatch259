package PR;

import java.util.Scanner;

public class PR06 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.print("Silahkan Ketik : ");
		
		String soal06 = input.nextLine();
		//split untuk memecah kata menjadi 1 index setelah spasi
		String[] tempSplit = soal06.split(" ");
		String A = "";
		
		
		for (int i = 0; i < tempSplit.length; i++) {
			char[] kata = tempSplit[i].toCharArray();
			for (int j = 0; j < kata.length; j++) {
				if (j % 2 == 1) {
					A += "*";
				}else {
					A += kata[j];
			}
			}
			A += " ";
		}
		System.out.print(A);
	}

}
