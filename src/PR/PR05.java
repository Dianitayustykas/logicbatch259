package PR;

import java.util.Arrays;
import java.util.Scanner;

public class PR05 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.println("Masukan kata = ");
		String kata = input.nextLine().toLowerCase().replaceAll(" ", "");
		// replaceAll digunakan untuk mengganti suatu karakter menjadi apa yang kita
		// inginkan

		String[] temp = kata.split(" ");
		char[] array = kata.toCharArray();
		String vokal = "";
		String konsonan = "";

		Arrays.sort(array);
		for (int i = 0; i < array.length; i++) {
			if (array[i] == 'a' || array[i] == 'i' || array[i] == 'u' || array[i] == 'e' || array[i] == 'o') {
				vokal += array[i];
			} else {
				konsonan += array[i];
			}

		}
		System.out.println("huruf vokal = " + vokal);
		System.out.println("huruf konsonan = " + konsonan);
		System.out.println(Arrays.toString(array));

	}
}
