package string;

import java.util.Scanner;

public class no09 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		String kata1 = input.next();
		String kata2 = input.next();
		
		char[] arr1 = new char[kata1.length()];
		char[] arr2 = new char[kata2.length()];
		
		for (int i = 0; i < kata1.length(); i++) {
			arr1[i] = kata1.charAt(i);
		}
		for (int i = 0; i < kata2.length(); i++) {
			arr2[i] = kata2.charAt(i);
		}
		int count = 0;
		for (int i = 0; i < kata1.length(); i++) {
			for (int j = 0; j < kata2.length(); j++) {
				if (arr1[i] == arr2[j]) {
					count++;
					arr2[j] = 0;
					break;
				}
			}
			
		}
		System.out.print((kata1.length()+kata2.length()) - (2*count));
	}

}
