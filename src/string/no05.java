package string;

import java.util.Scanner;

public class no05 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		int q = input.nextInt();
		while (q-- > 0) {
			String s1 = input.next();
			String s2 = "hack";
			int m = s1.length();
			int n = s2.length();
			
			char[] a = s1.toCharArray();
			char[] b = s2.toCharArray();
			
			int[][] c = new int[n+1][m+1];
			
			for (int i = 0; i <= n; i++) {
				for (int j = 0; j <= m; j++) {
					if (b[i-1] == a[j-1]) {
						c[i][j] = c[i-1][j-1]+1;
					}else {
						c[i][j] = Math.max(c [i-1][j], c [i][j-1]);
					}
				}
			}
			int result = c[n][m];
			if (result == n) {
				System.out.print("YES");
			}else {
				System.out.print("NO");
			}
		}

	}

}
