package string;

import java.util.Scanner;

public class no04 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		System.out.println("Masukan Kode : ");
		String a = input.next().toUpperCase();
		char[] huruf = a.toCharArray();
		
		int count = 0;
		
		for (int i = 0; i < a.length(); i = i+3) {
			if (huruf[i] != 'S' || huruf [i+1] != 'O' || huruf [i+2] != 'S') {
				count++;
			}
		}
		System.out.println(count);
	}

}
