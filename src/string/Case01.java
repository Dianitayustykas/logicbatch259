package string;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Case01 {
	public static void Resolve1() {
		Scanner input = new Scanner(System.in);
		System.out.print("Masukan Kata : ");

		String a = input.nextLine();
		String result = "";
		int hitung = 0;

		for (int i = 0; i < a.length(); i++) {
			char kata = a.charAt(i);
			if (Character.isUpperCase(kata)) {
				hitung++;
			}
		}
		System.out.println(hitung + 1);
	}

	public static void Resolve2() {
		Scanner input = new Scanner(System.in);

		System.out.print("Panjang Password : ");
		int n = input.nextInt();
		String password = input.nextLine();

		char[] huruf = password.toCharArray();

		int number = 0;
		int lower = 0;
		int upper = 0;
		int special = 0;
		int count = 0;

		for (int i = 0; i < password.length(); i++) {
			if (huruf[i] >= 48 && huruf[i] <= 57) {
				number++;
			} else if (huruf[i] >= 65 && huruf[i] <= 90) {
				upper++;
			} else if (huruf[i] >= 97 && huruf[i] <= 122) {
				lower++;
			} else {
				special++;
			}
		}
		if (number == 0) {
			count++;
		}
		if (upper == 0) {
			count++;
		}
		if (lower == 0) {
			count++;
		}
		if (special == 0) {
			count++;
		}
		if (password.length() >= 6) {
			System.out.print(count);
		} else {
			System.out.print(count + (6 - (password.length() + count)));
		}
	}

	public static void Resolve3() {
		Scanner input = new Scanner(System.in);

		System.out.print("Kata Pertama : ");
		String kataPertama = input.nextLine();
		System.out.print("Password : ");
		int password = input.nextInt();

		String kataAkhir = "";
		char huruf;

		for (int i = 0; i < kataPertama.length(); i++) {
			huruf = kataPertama.charAt(i);
			if (huruf >= 'a' && huruf <= 'z') {
				huruf = (char) (huruf + password);
				if (huruf < 'z') {
					huruf = (char) (huruf + 'a' - 'z' - 1);
				}
				kataAkhir = kataAkhir + huruf;
			} else if (huruf >= 'A' && huruf <= 'z') {
				huruf = (char) (huruf + password);
				if (huruf > 'z') {
					huruf = (char) (huruf + 'A' - 'z' - 1);
				}
				kataAkhir = kataAkhir + huruf;
			} else {
				kataAkhir = kataAkhir + huruf;
			}
		}
		System.out.print("Kata Terakhir : " + kataAkhir);
	}

	public static void Resolve4() {
		Scanner input = new Scanner(System.in);

		System.out.println("Masukan Kode : ");
		String a = input.next().toUpperCase();
		char[] huruf = a.toCharArray();

		int count = 0;

		for (int i = 0; i < a.length(); i = i + 3) {
			if (huruf[i] != 'S' || huruf[i + 1] != 'O' || huruf[i + 2] != 'S') {
				count++;
			}
		}
		System.out.println(count);
	}

	public static void Resolve5() {
		Scanner input = new Scanner(System.in);

		int q = input.nextInt();
		while (q-- > 0) {
			String s1 = input.next();
			String s2 = "hack";
			int m = s1.length();
			int n = s2.length();

			char[] a = s1.toCharArray();
			char[] b = s2.toCharArray();

			int[][] c = new int[n + 1][m + 1];

			for (int i = 0; i <= n; i++) {
				for (int j = 0; j <= m; j++) {
					if (b[i - 1] == a[j - 1]) {
						c[i][j] = c[i - 1][j - 1] + 1;
					} else {
						c[i][j] = Math.max(c[i - 1][j], c[i][j - 1]);
					}
				}
			}
			int result = c[n][m];
			if (result == n) {
				System.out.print("YES");
			} else {
				System.out.print("NO");
			}
		}

	}

	public static void Resolve6() {
		Scanner input = new Scanner(System.in);

		String a = input.nextLine();

		for (int i = 0; i < 26; i++) {
			int lower = (char) 'a';
			int upper = (char) 'A';
			for (int j = 0; j < a.length(); j++) {
				if (a.charAt(j) == (lower + i) || a.charAt(j) == (upper + i)) {
					break;
				} else if (j == a.length() - 1 && (a.charAt(j) != (lower + i) || a.charAt(j) != (upper + i))) {
					System.out.print("Not Pangram");
				}
			}
		}
		System.out.print("Pangram");
	}

	public static void Resolve8() {
		Scanner input = new Scanner(System.in);
		int numberM = input.nextInt();

		String inputChar = "";
		int[] count = new int[26];
		int[] temp;

		for (int i = 0; i < numberM; i++) {
			temp = new int[26];
			inputChar = input.next();
			for (int c : inputChar.toCharArray()) {
				temp[c - 97]++;
				if (temp[c - 97] == 1)
					count[c - 97]++;
			}
			temp = null;
		}
		int sum = 0;
		for (int i = 0; i < 26; i++) {
			if (count[i] == numberM)
				sum += 1;
		}
		count = null;
		System.out.println(sum);
	}

	public static void Resolve9() {
		Scanner input = new Scanner(System.in);

		String kata1 = input.next();
		String kata2 = input.next();

		char[] arr1 = new char[kata1.length()];
		char[] arr2 = new char[kata2.length()];

		for (int i = 0; i < kata1.length(); i++) {
			arr1[i] = kata1.charAt(i);
		}
		for (int i = 0; i < kata2.length(); i++) {
			arr2[i] = kata2.charAt(i);
		}
		int count = 0;
		for (int i = 0; i < kata1.length(); i++) {
			for (int j = 0; j < kata2.length(); j++) {
				if (arr1[i] == arr2[j]) {
					count++;
					arr2[j] = 0;
					break;
				}
			}

		}
		System.out.print((kata1.length() + kata2.length()) - (2 * count));
	}

	public static void Resolve10() {
		Scanner input = new Scanner(System.in);
		Set<Character> s1Set = new HashSet<>();
		Set<Character> s2Set = new HashSet<>();
		System.out.print("Masukan kata pertama : ");
		String s1 = input.nextLine();
		for (char c : s1.toCharArray()) {
			s1Set.add(c);
		}
		System.out.print("Masukan kata kedua : ");
		String s2 = input.nextLine();
		for (char c : s2.toCharArray()) {
			s2Set.add(c);
		}
		s1Set.retainAll(s2Set);
		if (!s1Set.isEmpty()) {
			System.out.print("yes!");
		} else {
			System.out.print("no!");
		}

	}
}
