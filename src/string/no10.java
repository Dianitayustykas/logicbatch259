package string;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class no10 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		Set<Character> s1Set = new HashSet<>();
		Set<Character> s2Set = new HashSet<>();
		System.out.print("Masukan kata pertama : ");
		String s1 = input.nextLine();
		for (char c : s1.toCharArray()) {
			s1Set.add(c);
		}
		System.out.print("Masukan kata kedua : ");
		String s2 = input.nextLine();
		for (char c : s2.toCharArray()) {
			s2Set.add(c);
		}
		s1Set.retainAll(s2Set);
		if (!s1Set.isEmpty()) {
			System.out.print("yes!");
		}else {
			System.out.print("no!");
		}
//		System.out.print("Masukan kata pertama : ");
		
	}

}
