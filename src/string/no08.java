package string;

import java.util.Scanner;

public class no08 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int numberM = input.nextInt();

		String inputChar = "";
		int[] count = new int[26];
		int[] temp;

		for (int i = 0; i < numberM; i++) {
			temp = new int[26];
			inputChar = input.next();
			for (int c : inputChar.toCharArray()) {
				temp[c - 97]++;
				if (temp[c - 97] == 1)
					count[c - 97]++;
			}
			temp = null;
		}
		int sum = 0;
		for (int i = 0; i < 26; i++) {
			if (count[i] == numberM)
				sum += 1;
		}
		count = null;
		System.out.println(sum);
	}

}
