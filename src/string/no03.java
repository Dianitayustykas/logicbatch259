package string;

import java.util.Scanner;

public class no03 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		System.out.print("Kata Pertama : ");
		String kataPertama = input.nextLine();
		System.out.print("Password : ");
		int password = input.nextInt();

		String kataAkhir = "";
		char huruf;

		for (int i = 0; i < kataPertama.length(); i++) {
			huruf = kataPertama.charAt(i);
			if (huruf >= 'a' && huruf <= 'z') {
				huruf = (char) (huruf + password);
				if (huruf < 'z') {
					huruf = (char) (huruf + 'a' - 'z' - 1);
				}
				kataAkhir = kataAkhir + huruf;
			} else if (huruf >= 'A' && huruf <= 'z') {
				huruf = (char) (huruf + password);
				if (huruf > 'z') {
					huruf = (char) (huruf + 'A' - 'z' - 1);
				}
				kataAkhir = kataAkhir + huruf;
			} else {
				kataAkhir = kataAkhir + huruf;
			}
		}
		System.out.print("Kata Terakhir : " + kataAkhir);
	}

}
