import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

public class PR05 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.print("Silahkan Ketik : ");

		//replaceAll untuk mengganti suatu karakter menjadi apa yang diinginkan
		//toLowerCase 
		String kata = input.nextLine().toLowerCase().replaceAll(" ", "");
		System.out.print("");
		
		char[] huruf = kata.toCharArray();
		String vokal = "";
		String konsonan = "";
		String urutan = "";

		Arrays.sort(huruf);
		for (int i = 0; i < huruf.length; i++) {
			if (huruf[i] == 'a' || huruf[i] == 'i' || huruf[i] == 'u' 
					|| huruf[i] == 'e' || huruf[i] == 'o') {
				vokal += huruf[i];
			} else {
				konsonan += huruf[i];
			}
//			for (char urutanHuruf : huruf) {
//				urutan = "" + urutanHuruf;
//			}
		}
		System.out.println("Huruf Vokal : " + vokal);
		System.out.println("Huruf Konsonan : " + konsonan);
		System.out.println(Arrays.toString(huruf));
	}
}