package PR01;

import java.text.*;
import java.util.*;

public class jawaban04 {

	public static void main(String[] args) throws ParseException {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		System.out.print("Masukan Waktu : ");
		String A = input.nextLine().toLowerCase();
		
		DateFormat formatAwal = new SimpleDateFormat("hh:mm:ssaa");
		DateFormat formatAkhir = new SimpleDateFormat("hh:mm:ss");
	
			Date waktu = formatAwal.parse(A);
			String hasil = formatAkhir.format(waktu);
			System.out.println(hasil);
		
	}

}
