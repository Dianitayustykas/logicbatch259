package PR01;

import java.util.Scanner;

public class jawaban03 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.print("n : ");
		int n = input.nextInt();
		boolean bilangan;
		
		//dimulai dari idx ke 2 karena idx ke 0 dan 1 bukan bil prima
		for (int i = 2; i < n; i++) {
			bilangan = true;
			for (int j = 2; j < i; j++) {
				if(i%j == 0) {
					bilangan = false;
					break;
				}
			}
			if(bilangan == true) {
				System.out.print(i+",");
			}
		} 
	}

}
