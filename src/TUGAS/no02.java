package TUGAS;

import java.text.SimpleDateFormat;
import java.util.Scanner;

public class no02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.print("Tanggal Pinjam = ");
		String stglAwal = input.next();
		System.out.print("Tanggal Pengembalian = ");
		String stglAkhir = input.next();

		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

		java.util.Date d1 = null;
		java.util.Date d2 = null;

		try {
			d1 = format.parse(stglAwal);
			d2 = format.parse(stglAkhir);

			long diff = d2.getTime() - d1.getTime();
			long diffDays = diff / (24 * 60 * 60 * 1000);
			long telat = diffDays - 3;
			System.out.println("Telat = " + telat + "hari");
			System.out.println("Denda = " + (telat * 500));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
