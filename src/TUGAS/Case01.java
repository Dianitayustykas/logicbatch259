package TUGAS;

import java.text.SimpleDateFormat;
import java.util.Scanner;

public class Case01 {
	public static void Resolve1() {
		Scanner input = new Scanner(System.in);

		System.out.print("Jam Masuk = ");
		String masuk = input.next().trim();
		String jamMasuk = masuk.substring(0, 2);
		String menitMasuk = masuk.substring(3);

		System.out.print("Jam Keluar = ");
		String keluar = input.next();
		String jamKeluar = keluar.substring(0, 2);
		String menitKeluar = masuk.substring(3);

		// setelah itu di ganti ke integer
		int jamMasukInt = Integer.parseInt(jamMasuk);
		int menitMasukInt = Integer.parseInt(menitMasuk);
		int jamKeluarInt = Integer.parseInt(jamKeluar);
		int menitKeluarInt = Integer.parseInt(menitKeluar);

		// konverensi ke detik
		int waktuMasuk = (3600 * jamMasukInt) + (60 * menitMasukInt);
		int waktuKeluar = (3600 * jamKeluarInt) + (60 * menitKeluarInt);
		int totalDetik = waktuKeluar - waktuMasuk;
		int totalJam = totalDetik / 3600;
		int sisa = totalDetik % 3600;
		int menit = sisa / 60;

		System.out.println("Lama parkir = " + totalJam + "jam" + menit + "menit");

		if (menit > 30) {
			totalJam = totalJam + 1;
		}
		int tarif = totalJam * 3000;
		System.out.println("Tarif = Rp. " + tarif);

	}

	public static void Resolve2() {
		Scanner input = new Scanner(System.in);
		System.out.print("Tanggal Pinjam = ");
		String stglAwal = input.next();
		System.out.print("Tanggal Pengembalian = ");
		String stglAkhir = input.next();

		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

		java.util.Date d1 = null;
		java.util.Date d2 = null;

		try {
			d1 = format.parse(stglAwal);
			d2 = format.parse(stglAkhir);

			long diff = d2.getTime() - d1.getTime();
			long diffDays = diff / (24 * 60 * 60 * 1000);
			long telat = diffDays - 3;
			System.out.println("Telat = " + telat + "hari");
			System.out.println("Denda = " + (telat * 500));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void Resolve3() {
		Scanner input = new Scanner(System.in);
		System.out.print("Tanggal Mulai = ");
		
	}

	public static void Resolve4() {
		Scanner input = new Scanner(System.in);
		System.out.println("Masukan Kata = ");
		String kataAwal = input.next();

		int panjang = kataAwal.length();

		String kataBalik = "";
		
		for (int i = panjang - 1; i >= 0; i--) {
			kataBalik = kataBalik + kataAwal.charAt(i);
		}
		if (kataAwal.equals(kataBalik)) {
			System.out.println("Yes!");
		} else {
			System.out.println("No!");
		}
	}
}
