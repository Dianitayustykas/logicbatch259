package TUGAS;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class no01 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.print("Jam Masuk = ");
		String masuk = input.nextLine().toLowerCase();

		System.out.println("Waktu Keluar : ");
		String keluar = input.nextLine().toLowerCase();

		DateFormat formatAwal = new SimpleDateFormat("dd/MM/yyyy hh:mm");

		Date jamMasuk = null;
		Date jamKeluar = null;

		try {
			jamMasuk = formatAwal.parse(masuk);
			jamKeluar = formatAwal.parse(keluar);
			double diff = jamKeluar.getTime() - jamMasuk.getTime();
			double diff2 = diff / (60 * 60 * 1000);
			int sumParse = (int) Math.ceil(diff2);
			int sumPembayaran = sumParse * 3000;
			System.out.println(sumPembayaran);
		} catch (ParseException x) {
			x.printStackTrace();
		}
	}

}
