package day2;

import java.util.Scanner;

public class Case01 {
	public static void Resolve1() {
		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan nilai n : ");
		int n = input.nextInt();
		System.out.print("Masukkan nilai n2 : ");
		int n2 = input.nextInt();

		int x = 1;

		int[][] tempArray = new int[2][n];

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0) {
					tempArray[i][j] = j;
					System.out.print(tempArray[i][j] + " ");
				} else {
					tempArray[i][j] = x;
					x *= n2;
					System.out.print(tempArray[i][j] + " ");
				}
			}
			System.out.println();
		}
		input.close();
	}

	public static void Resolve2() {
		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan nilai n : ");
		int n = input.nextInt();
		System.out.print("Masukkan nilai n2 : ");
		int n2 = input.nextInt();

		int x = 1;

		int[][] tempArray = new int[2][n];
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0) {
					tempArray[i][j] = j;
					System.out.print(tempArray[i][j] + " ");
				} else if (j % 3 == 2) {
					tempArray[i][j] = x * -1;
					System.out.print(tempArray[i][j] + " ");
					x *= n2;
				} else {
					tempArray[i][j] = x;
					System.out.print(tempArray[i][j] + " ");
					x *= n2;
				}
			}
			System.out.println();
		}
		input.close();
	}

	public static void Resolve3() {
		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan nilai n : ");
		int n = input.nextInt();
		System.out.print("Masukkan nilai n2 : ");
		int n2 = input.nextInt();
		
		int[][] tempArray = new int[2][n];
		int x = 0;
		
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0) {
					tempArray[i][j] = j;
					System.out.print(tempArray[i][j] + " ");
				} else if (j >= 3) {
					tempArray[i][j] = x;
					System.out.print(tempArray[1][j] + " ");
					x = 1 * 2;
				} else {
					tempArray[i][j] = x;
					x *= 2;
					System.out.print(tempArray[i][j] + " ");
				}
			}
			System.out.println();
		}
		input.close();
	}

	public static void Resolve4() {
		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan nilai n : ");
		int n = input.nextInt();
		System.out.print("Masukkan nilai n2 : ");
		int n2 = input.nextInt();
		
		int[][] tempArray = new int[2][n];
		int x = 1;

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0) {
					tempArray[i][j] = j;
					System.out.print(tempArray[i][j] + " ");
				} else if (j % 2 == 0) {
					tempArray[i][j] = x;
					System.out.print(tempArray[i][j] + " ");
					x += 1;
				} else {
					tempArray[i][j] = n2;
					System.out.print(tempArray[i][j] + " ");
					n2 += n2;
				}
			}
			System.out.println();
		}
		input.close();
	}

	public static void Resolve5() {
		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan nilai n : ");
		int n = input.nextInt();

		int[][] tempArray = new int[3][n];
		int x = 0;

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < n; j++) {
				tempArray[i][j] = x;
				System.out.print(tempArray[i][j] + " ");
				x += 1;
			}
			System.out.println();
		}
		input.close();
	}

	public static void Resolve6() {
		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan nilai n : ");
		int n = input.nextInt();

		int[][] tempArray = new int[3][n];
		int x = 1;
		int b = 0;

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0) {
					tempArray[i][j] = j;
					System.out.print(tempArray[i][j] + " ");
				} else if (i == 1) {
					tempArray[i][j] = x;
					System.out.print(tempArray[i][j] + " ");
					x *= n;
				} else {
					b = (tempArray[0][j] + tempArray[1][j]);
					tempArray[i][j] = b;
					System.out.print(tempArray[i][j] + " ");
				}
			}
			System.out.println();
		}
		input.close();
	}

	public static void Resolve7() {
		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan nilai n : ");
		int n = input.nextInt();

		int[][] tempArray = new int[3][n];
		int x = 0;

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < n; j++) {
				tempArray[i][j] = x;
				System.out.print(tempArray[i][j] + " ");
				x += 1;
			}
			System.out.println();
		}
		input.close();
	}

	public static void Resolve8() {
		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan nilai n : ");
		int n = input.nextInt();

		int[][] tempArray = new int[3][n];
		int x = 0;
		int b = 1;

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0) {
					tempArray[i][j] = j;
					System.out.print(tempArray[i][j] + " ");
				} else if (i == 1) {
					tempArray[i][j] = x;
					System.out.print(tempArray[i][j] + " ");
					x += 2;
				} else {
					b = (tempArray[0][j] + tempArray[1][j]);
					tempArray[i][j] = b;
					System.out.print(tempArray[i][j] + " ");
				}
			}
			System.out.println();
		}
		input.close();
	}

	public static void Resolve9() {
		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan nilai n : ");
		int n = input.nextInt();
		System.out.print("Masukkan nilai n2 : ");
		int n2 = input.nextInt();
		
		int[][] tempArray = new int[3][n];
		int x = 0;
		
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0) {
					tempArray[i][j] = j;
					System.out.print(tempArray[i][j] + " ");
				} else if (i == 1) {
					tempArray[i][j] = x;
					System.out.print(tempArray[i][j] + " ");
					x += 3;
				} else {
					tempArray[i][j]= x-n2;
					System.out.print(tempArray[i][j] + " ");
					x-=n2;
				}
			}
			System.out.println();
		}
		input.close();
	}

	
	public static void Resolve10() {
		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan nilai n : ");
		int n = input.nextInt();
		System.out.print("Masukkan nilai n2 : ");
		int n2 = input.nextInt();
		
		int[][] tempArray = new int[3][n];
		int x = 0;
		
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0) {
					tempArray[i][j] = j;
					System.out.print(tempArray[i][j] + " ");
				} else if (i == 1) {
					tempArray[i][j] = x;
					System.out.print(tempArray[i][j] + " ");
					x += 3;
				} else {
					x = (tempArray[0][j] + tempArray[1][j]);
					tempArray[i][j] = x;
					System.out.print(tempArray[i][j] + " ");
				}
			}
			System.out.println();
		}
		input.close();
	}
}
