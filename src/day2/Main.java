package day2;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String answer = "Y";
		Scanner input = new Scanner(System.in);
		
		while (answer.toUpperCase().equals("Y")) {
			try {
				System.out.println("Enter the number of case");
				int number = input.nextInt();
				input.nextLine();
				
				switch (number) {
				case 1 :
					Case01.Resolve1();
					break;
				case 2 :
					Case01.Resolve2();
					break;
				case 3 :
					Case01.Resolve3();
					break;
				case 4 :
					Case01.Resolve4();
					break;
				case 5 :
					Case01.Resolve5();
					break;
				case 6 :
					Case01.Resolve6();
					break;
				case 7 :
					Case01.Resolve7();
					break;
				case 8 :
					Case01.Resolve8();
					break;
				case 9 :
					Case01.Resolve9();
					break;
				case 10 :
					Case01.Resolve10();
					break;
				default :
					System.out.println("Case not a available");
					break;
				}
			}catch (Exception e) {
				System.out.println(e.getMessage());
			}
			System.out.println();
			System.out.println("Continue?");
			answer = input.nextLine();
			}
			input.close();
	}
}
