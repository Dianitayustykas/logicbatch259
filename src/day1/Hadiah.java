package day1;

import java.util.Scanner;

public class Hadiah {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner (System.in);
		System.out.println("Pilih Nomer Hadiah ");
		
		int gift = input.nextInt();
		String giftString = "";
		
		switch (gift) {
		case 1 :
			giftString = "Tisu";
			break;
		case 2 :
			giftString = "Kopi Kenangan";
			break;
		case 3 :
			giftString = "Susu Kental Manis";
			break;
		case 4 :
			giftString = "Minyak Goreng";  
			break;
		case 5 :
			giftString = "Uang Rp.50.000";
			break;
		default:
			System.out.println("Nomer Hadiah yang Anda Pilih Salah!");
		}
		if (giftString.equals ("") ) {
			System.out.println("Kamu Gagal Mendapatkan Hadiah");
		}else {
			System.out.println("Selamat kamu mendapatkan hadiah : " + giftString);
		}
	}
	
}
