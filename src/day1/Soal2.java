package day1;

import java.util.Scanner;

public class Soal2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.print("Belanja : ");
		int belanja = input.nextInt();
		System.out.print("Jarak : ");
		int jarak = input.nextInt();
		System.out.print("Masukan Promo : ");
		String kodePromo = input.next();

		int diskon = 0;
		int ongkir = 0;

		if (kodePromo.equals("JKTOVO")) {
			if (belanja >= 30000) {
				diskon = belanja * (40) / (100);
			} else if (belanja < 30000) {
				diskon = 0;
			}
		} else {
			System.out.println("Kode Promo yang anda masukan Salah!");
		}
		if (diskon >= 30000) {
			diskon = 30000;
		}
		if (jarak <= 5) {
			ongkir = 5000;
		} else {
			ongkir = (jarak * 1000);
		}
		System.out.println("Diskon 40% : " + diskon);
		System.out.println("Ongkir : " + ongkir);
		System.out.println("Total Belanja : " + (belanja - diskon + ongkir));
	}

}
