package day1;

import java.util.Scanner;

public class Soal3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.print("Belanja : ");
		int belanja = input.nextInt();
		System.out.print("Ongkos Kirim : ");
		int ongkir = input.nextInt();
		
		int diskonBelanja = 0;
		int diskonOngkir = 0;
		
		if (belanja >= 30000) {
			diskonOngkir = 5000;
			diskonBelanja = 5000;
		}else if (belanja >= 50000) {
			diskonOngkir = 10000;
			diskonBelanja = 10000;
		}else {
			diskonOngkir = 10000;
			diskonBelanja = 20000;
		}
		System.out.println("Ongkos Kirim : " + ongkir);
		System.out.println("Diskon Ongkir : " + diskonOngkir );
		System.out.println("Diskon Belanja : " + diskonBelanja);
		System.out.println("Total Belanja : " + (belanja - diskonBelanja + ongkir - diskonOngkir));
	}

}
